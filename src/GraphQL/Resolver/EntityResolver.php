<?php
namespace App\GraphQL\Resolver;
use App\Entity\Entity;
use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Relay\Connection\Output\Connection;
use Overblog\GraphQLBundle\Relay\Connection\Paginator;
Class EntityResolver implements ResolverInterface, AliasedInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    public  function findAllEntities()
    {
        $entities = $this->em->getRepository(Entity::class)->findAll();
         return $entities;
    }
    public  function findById(Argument $args)
    {
        $entity = $this->em->getRepository(Entity::class)->findOneById($args['id']);
         return $entity;
    }
    public static function getAliases(): array
    {
        return [
            'findAllEntities' => 'all_entities',
            'findById' => 'entity_by_id'
        ];
    }
}