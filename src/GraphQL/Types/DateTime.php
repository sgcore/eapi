<?php
namespace App\GraphQL\Types;
use GraphQL\Language\AST\BooleanValueNode;
use GraphQL\Language\AST\FloatValueNode;
use GraphQL\Language\AST\IntValueNode;
use GraphQL\Language\AST\ListValueNode;
use GraphQL\Language\AST\Node;
use GraphQL\Language\AST\ObjectValueNode;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;

class DateTime extends ScalarType
{
    public $name = 'DateTime';
    

    
   public function parseValue($value)
    {
        return $this->identity($value);
    }

    public function serialize($value)
    {
        return json_encode($value);
    }
    public function parseLiteral($valueNode, ?array $variables = null)
    {
        return $valueNode->value;

    }

    private function identity($value)
    {
        return $value;
    }

}