<?php
namespace App\GraphQL\Mutation;
use App\Entity\Entity;
use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Relay\Connection\Output\Connection;
use Overblog\GraphQLBundle\Relay\Connection\Paginator;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
Class EntityMutation implements MutationInterface, AliasedInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;
    protected $currentUser;
    public function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage)
    {
        $this->em = $em;
        $this->currentUser = $tokenStorage->getToken()->getUser();
    }
    public  function putEntity(Argument $args)
    {
        $rawArgs = $args->getRawArguments();
        $entity = false;
        if (array_key_exists("id",$rawArgs['input']))
        {
            $id = $rawArgs['input']['id'];
            // id and code can not change
            unset($rawArgs['input']['id']);
            unset($rawArgs['input']['code']);
            $entity = $this->em->getRepository(Entity::class)->findOneById($id);
        }
       
        
        if(!$entity)
        {
            $entity = new Entity();
            $entity->setCode(uniqid('', true));
        }
        
        foreach($rawArgs['input'] as $key => $value)
        {
            $setter = sprintf('set%s', $key);
            if($key == 'content')
            {
                $value = stripcslashes ($value);
            }
            if($key == 'parent')
            {
                $value = $this->em->getRepository(Entity::class)->findOneById($value);
            }
            $entity->$setter($value);
        }
        $entity->setUser($this->currentUser);
        $this->em->persist($entity);
        $this->em->flush();
         return $entity;
    }
    public static function getAliases(): array
    {
        return [
            'putEntity' => 'put_entity'
        ];
    }
}