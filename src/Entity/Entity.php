<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use App\Entity\User;
use DateTime;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EntityRepository")
 * @JMS\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks
 */
class Entity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Groups({"default", "basic", "invoice"})
     * @JMS\Expose
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @JMS\Groups({"default", "basic", "invoice"})
     * @JMS\Expose
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Groups({"default", "basic", "invoice"})
     * @JMS\Expose
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=16)
     * @JMS\Groups({"default", "basic", "invoice"})
     * @JMS\Expose
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     * @JMS\Groups({"default", "basic", "invoice"})
     * @JMS\Expose
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     * @JMS\Groups({"default", "basic", "invoice"})
     * @JMS\Expose
     */
    private $image;

    /**
     * @ORM\Column(type="json", nullable=true)
     * @JMS\Groups({"default"})
     * @JMS\Expose
     */
    private $content = [];

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     * @JMS\Groups({"default", "basic", "invoice"})
     * @JMS\Expose
     */
    private $status;

    /**
     * @ORM\Column(type="decimal", nullable=true, scale=3)
     * @JMS\Groups({ "invoice"})
     * @JMS\Expose
     */
    private $amount;


    /**
     * @var DateTime $created
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @JMS\Groups({"invoice"})
     * @JMS\SerializedName("date")
     * @JMS\Expose
     */
    protected $createdAt;

    /**
     * @var DateTime $updated
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    protected $updatedAt;

    

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Entity", inversedBy="childrens", fetch="LAZY")
     */
    private $parent;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", fetch="LAZY")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Entity", mappedBy="parent", fetch="LAZY")
     */
    private $childrens;

    public function __construct()
    {
        $this->childrens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getAmount(): ?Float
    {
        return $this->amount;
    }

    public function setAmount(?Float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $dateTimeNow = new DateTime('now');

        $this->setUpdatedAt($dateTimeNow);

        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($dateTimeNow);
        }
    }

    public function getCreatedAt() :?DateTime
    {
        return $this->createdAt;
    }
    
    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt() :?DateTime
    {
        return $this->updatedAt;
    }
    
    public function setUpdatedAt(DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildrens(): Collection
    {
        return $this->childrens;
    }

    public function addChildren(self $children): self
    {
        if (!$this->childrens->contains($children)) {
            $this->childrens[] = $children;
            $children->setParent($this);
        }

        return $this;
    }

    public function removeChildren(self $children): self
    {
        if ($this->childrens->contains($children)) {
            $this->childrens->removeElement($children);
            // set the owning side to null (unless already changed)
            if ($children->getParent() === $this) {
                $children->setParent(null);
            }
        }

        return $this;
    }
}
