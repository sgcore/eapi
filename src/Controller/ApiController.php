<?php
namespace App\Controller;

use App\Entity\User;
use App\Entity\Entity;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Swagger\Annotations as SWG;

/**
 * Class ApiController
 *
 * @Route("/api")
 */
class ApiController extends AbstractController
{
    // USER URI's
 
    /**
     * @Rest\Post("/login_check", name="user_login_check")
     *
     * @SWG\Response(
     *     response=200,
     *     description="User was logged in successfully"
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="User was not logged in successfully"
     * )
     *
     * @SWG\Parameter(
     *     name="_username",
     *     in="body",
     *     type="string",
     *     description="The username",
     *     schema={
     *     }
     * )
     *
     * @SWG\Parameter(
     *     name="_password",
     *     in="body",
     *     type="string",
     *     description="The password",
     *     schema={}
     * )
     *
     * @SWG\Tag(name="User")
     */
    public function getLoginCheckAction()
    {
    }
 
    /**
     * @Rest\Post("/register", name="user_register")
     *
     * @SWG\Response(
     *     response=201,
     *     description="User was successfully registered"
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="User was not successfully registered"
     * )
     *
     * @SWG\Parameter(
     *     name="_name",
     *     in="body",
     *     type="string",
     *     description="The username",
     *     schema={}
     * )
     *
     * @SWG\Parameter(
     *     name="_email",
     *     in="body",
     *     type="string",
     *     description="The username",
     *     schema={}
     * )
     *
     * @SWG\Parameter(
     *     name="_username",
     *     in="body",
     *     type="string",
     *     description="The username",
     *     schema={}
     * )
     *
     * @SWG\Parameter(
     *     name="_password",
     *     in="query",
     *     type="string",
     *     description="The password"
     * )
     *
     * @SWG\Tag(name="User")
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        $em = $this->getDoctrine()->getManager();
 
        $user = [];
        $message = "";
 
        try {
            $code = 200;
            $error = false;
 
            $name = $request->request->get('_name');
            $dni = $request->request->get('_dni');
            $email = $request->request->get('_email');
            $username = $request->request->get('_username');
            $password = $request->request->get('_password');
 
            $user = new User();
            $user->setName($name);
            $user->setDni($dni);
            $user->setEmail($email);
            $user->setUsername($username);
            $user->setPlainPassword($password);
            $user->setPassword($encoder->encodePassword($user, $password));
 
            $em->persist($user);
            $em->flush();
        } catch (UniqueConstraintViolationException $ex) {
            $code = 409;
            $error = true;
            $message = "Los datos ingresados pertenecen a otro usuario";
        } catch (Exception $ex) {
            $code = 500;
            $error = true;
            $message = "An error has occurred trying to register the user - Error: {$ex->getMessage()}";
        }
 
        $response = [
            'code' => $code,
            'error' => $error,
            'data' => $code == 200 ? $user : $message,
        ];
 
        return new Response($serializer->serialize($response, "json"), $code);
    }
    /**
     * @Route("/profile/{fid}", name="api", defaults={"id": 0})
     */
    public function profile(int $fid)
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        $em = $this->getDoctrine()->getManager();
        $me = $this->getUser();
        $invoices = [];
        $facilities = [];
        $isOwner = in_array('ROLE_OWNER',$me->getRoles());
        if($isOwner)
        {
            $facilities = $em->getRepository(Entity::class)->findBy(array("user" => $me->getID(), "type"=>"facility" ),array('id' => 'ASC'));
            $ids = array_map(function ($f) {
                return $f->getId();
            }, $facilities);
            $invoices = $em->getRepository(Entity::class)->findBy(array("parent" => $ids, "type"=>"invoice" ),array('id' => 'ASC'));
        }else if($fid > 0)
        {
            $invoices = $em->getRepository(Entity::class)->findBy(array("parent" => $fid, "user" => $me->getID() ),array('id' => 'ASC'));
        }
       
        $response = [
            'name' => $me->getName(),
            'email' => $me->getEmail(),
            'dni' => $me->getDni(),
            'id' => $me->getID(),
            'isOwner' => $isOwner,
            'invoices' => $invoices,
            'facilities' => $facilities
        ];
 
        return new Response($serializer->serialize($response, "json", \JMS\Serializer\SerializationContext::create()->setGroups(array('invoice'))));
    }

    /**
     * @Route("/facility/{id}", name="api_facility", defaults={"id": 0})
     */
    public function facility(int $id)
    {
        $entity = $this->getDoctrine()->getManager()->getRepository(Entity::class)->findOneById($id);
       
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
       
 
        return new Response($serializer->serialize($entity , "json", \JMS\Serializer\SerializationContext::create()->setGroups(array('default'))));
    }
    /**
     * @Route("/facilities", name="api_facilities")
     */
    public function facilities()
    {
        $entity = $this->getDoctrine()->getManager()->getRepository(Entity::class)->findBy(["type"=>"facility"]);
       
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
       
 
        return new Response($serializer->serialize($entity , "json", \JMS\Serializer\SerializationContext::create()->setGroups(array('basic'))));
    }
}
